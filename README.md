# hangelec

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).


对应表名
日（月、年）————————————daily，monthly，annual
日冻结正向有功——————————Daily frozen positive active power————————————dailyFrozenPAP
日冻结正向无功——————————Daily frozen positive reactive power————————————dailyFrozenPRP
日冻结反向有功——————————Daily frozen reverse active power————————————dailyFrozenRAP
日冻结反向无功——————————Daily frozen reverse reactive power————————————dailyFrozenRAP
