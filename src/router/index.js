import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    component: () => import('@/views/loginPage.vue'),
    redirect: 'loginPage'
  },
  {
    path: '/loginPage',
    name: 'loginPage',
    component: () => import('@/views/loginPage.vue')
  },
  {
    path: '/navPage',
    name: 'navPage',
    component: () => import('@/views/navPage.vue'),
    children: [
      {
        path: '/ScreenPage',
        name: 'ScreenPage',
        component: () => import('@/views/ScreenPage.vue')
      },
      {
        path: '/eChartBarPage',
        name: 'eChartBarPage',
        component: () => import('@/views/eChartBarPage.vue')
      },
      {
        path: '/dailyFrozenPowerPage',
        name: 'dailyFrozenPowerPage',
        component: () => import('@/views/dailyFrozenPowerPage.vue')
      },
      {
        path: '/eChartSectorPage',
        name: 'eChartSectorPage',
        component: () => import('@/views/eChartSectorPage.vue')
      },
      {
        path: '/eChartLinePage',
        name: 'eChartLinePage',
        component: () => import('@/views/eChartLinePage.vue')
      },
      {
        path: '/eChartMapPage',
        name: 'eChartMapPage',
        component: () => import('@/views/eChartMapPage.vue')
      },
      {
        path: '/errorYearBarPage',
        name: 'errorYearBarPage',
        component: () => import('@/views/errorYearBarPage.vue')
      },
      {
        path: '/eChartLineErrorPage',
        name: 'eChartLineErrorPage',
        component: () => import('@/views/eChartLineErrorPage.vue')
      }
    ]
  }
]

const router = new VueRouter({
  routes
})

export default router
