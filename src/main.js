import Vue from 'vue'
import App from './App.vue'
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
import router from './router'
import store from './store'
import axios from 'axios'
// 引入字体文件
import './assets/font/iconfont.css'
// 引入全局样式文件
import './assets/css/global.less'
// 引入字体文件
// import './assets/font/iconfont.css'
// 请求基准路径的配置 后端地址为:http://192.168.1.2:8001/dfs/web/ 测试地址 http://127.0.0.1:8888/api/
axios.defaults.baseURL = 'http://127.0.0.1:8888/api/'

Vue.use(ElementUI)

// 将全局echarts对象挂载到Vue的原型对象上
// 别的组件中直接可以使用this.$echarts使用
// 别的组件中可以直接使用this.$http使用axios请求
Vue.prototype.$echarts = window.echarts
Vue.prototype.$http = axios

Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')

axios({
  url: ''
})
